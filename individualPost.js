var html = "";

(function () {
    var config = {
        max_results: 101,
        max_per_page: 1,
        page: 1
    },
        no_of_pages = Math.ceil(config.max_results / config.max_per_page),
        results = [];

    function init() {
        var x = 1;
        req = new XMLHttpRequest();
        req.open("GET", 'https://jsonplaceholder.typicode.com/posts', true);
        req.send();
        req.onload = function () {
            json = JSON.parse(req.responseText);
            var html1 = "";

            json.forEach(function (val) {
                var keys = Object.keys(val);
                html1 = "";
                html1 += "<div class = 'post'>";
                html1 += "<strong>Post:</strong><br>";
                keys.forEach(function (key) {
                    html1 += "<strong>" + key + "</strong>: " + val[key] + "<br>";
                });
                html1 += "</div><br>";
                results[x] = html1;
                x++;

            });

        };


        document.getElementById("next").onclick = function () {
            pager("next");
            return false;
        };
        document.getElementById("previous").onclick = function () {
            pager("previous");
            return false;
        };
        document.getElementById("reset").onclick = function () {
            pager("reset");
            return false;
        };
        document.getElementById("goto").onclick = function () {
            pager("goto", document.getElementById("page_input").value);
            return false;
        };
        document.getElementById("page_nav").onclick = function (e) {
            var page = e.srcElement.getAttribute("data-page");
            if (page) {
                pager("goto", page);
            }
            return false;
        };
        update_page();
    }

    function pager(action, page) {
        switch (action) {
            case "next":
                if ((config.page + 1) < no_of_pages) {
                    ++config.page;
                }
                break;

            case "previous":
                if ((config.page - 1) >= 1) {
                    --config.page;
                }
                break;

            case "goto":
                config.page = page;
                break;

            case "reset":
                config.page = 1;
                break;

            default:
                break;
        }
        update_page();
    }
    function build_nav() {
        var i,
            page_nav = "";

        for (i = config.page; i < no_of_pages; i++) {
            page_nav += "<li><a data-page=" + i + ">" + i + "</a></li>\n";
        }
        return page_nav;
    }
    function build_results() {
        var i,
            tmp = "",
            start = (config.page !== 1) ? config.page * config.max_per_page : 1,
            end = start + config.max_per_page,
            result;

        for (i = start; i < end; i++) {
            result = results[i];

            if (typeof result !== "undefined") {
                tmp += "<p>" + result + "</p>\n";
                SearchId(Math.ceil(config.page / 10));
            }
            else {
                tmp = "";
            }
        }
        return tmp;
    }
    function update_page() {
        document.getElementById("curr_page").innerText = config.page;
        document.getElementById("page_nav").innerHTML = build_nav();
        document.getElementById("results").innerHTML = build_results();
    }
    window.addEventListener("load", function () {
        init();
    });
})();


//Starts author details function..
function SearchId(filter) {
    var input, authorId;

    req = new XMLHttpRequest();
    var link = 'https://jsonplaceholder.typicode.com/users';
    var findByAuthor = '?id=' + filter;
    req.open("GET", link + findByAuthor, true);
    req.send();
    req.onload = function () {
        json = JSON.parse(req.responseText);
        html = "";
        json.forEach(function (val) {
            var keys = Object.keys(val);
            html += "<div class = 'post'>";
            html += "<strong>Author Details:</strong><br>";

            keys.forEach(function (key) {
                html += "<strong>" + key + "</strong>: " + val[key] + "<br>";
                if (key === 'id') {
                    authorId = val[key];
                }
                if (key === 'company') {
                    html += "<strong>Company:</strong>" + "<br>";
                    html += "<strong>" + "company-name" + "</strong>: " + val[key]['name'] + "<br>";
                    html += "<strong>" + "company-catchPhrase" + "</strong>: " + val[key]['catchPhrase'] + "<br>";
                    html += "<strong>" + "company-bs" + "</strong>: " + val[key]['bs'] + "<br>";
                }
                else if (key === 'address') {
                    html += "<strong>Address:</strong>" + "<br>";
                    html += "<strong>" + "street" + "</strong>: " + val[key]['street'] + "<br>";
                    html += "<strong>" + "suite" + "</strong>: " + val[key]['suite'] + "<br>";
                    html += "<strong>" + "city" + "</strong>: " + val[key]['city'] + "<br>";
                    html += "<strong>" + "zipcode" + "</strong>: " + val[key]['zipcode'] + "<br>";
                    html += "<strong>Geo-location:</strong>" + "<br>";
                    html += "<strong>" + "lat" + "</strong>: " + val[key]['geo']['lat'] + "<br>";
                    html += "<strong>" + "lng" + "</strong>: " + val[key]['geo']['lng'] + "<br>";
                }
            });
            html += "</div><br>";
        });
        document.getElementsByClassName('post-list')[0].innerHTML = html;


  //      return html;
    }

}

//Ends author info function

