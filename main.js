var author = "";
var userList = [];

//Pagination function starts

(function () {
    var config = {
        max_results: 110,
        max_per_page: 10,
        page: 1
    },
        no_of_pages = Math.ceil(config.max_results / config.max_per_page),
        results = [];

    function init() {
        var x = 1;
        req = new XMLHttpRequest();
        req.open("GET", 'https://jsonplaceholder.typicode.com/posts', true);
        req.send();
        req.onload = function () {
            json = JSON.parse(req.responseText);
            var html = "";

            json.forEach(function (val) {
                var keys = Object.keys(val);
                html = "";
                html += "<div class = 'post'>";

                keys.forEach(function (key) {
                    if(key !== 'body' && key !== 'userId')
                    html += "<strong>" + key + "</strong>: " + val[key] + "<br>";
                });
                html += "</div><br>";
                results[x] = html;
                x++;
                if (x === 11) { x += 10; }
            });

        };

        document.getElementById("next").onclick = function () {
            pager("next");
            return false;
        };
        document.getElementById("previous").onclick = function () {
            pager("previous");
            return false;
        };
        document.getElementById("reset").onclick = function () {
            pager("reset");
            return false;
        };
        document.getElementById("goto").onclick = function () {
            pager("goto", document.getElementById("page_input").value);
            return false;
        };
        document.getElementById("page_nav").onclick = function (e) {
            var page = e.srcElement.getAttribute("data-page");
            if (page) {
                pager("goto", page);
            }
            return false;
        };
        update_page();
    }

    function pager(action, page) {
        switch (action) {
            case "next":
                if ((config.page + 1) < no_of_pages) {
                    ++config.page;
                }
                break;

            case "previous":
                if ((config.page - 1) >= 1) {
                    --config.page;
                }
                break;

            case "goto":
                config.page = page;
                break;

            case "reset":
                config.page = 1;
                break;

            default:
                break;
        }
        update_page();
    }
    function build_nav() {
        var i,
            page_nav = "";

        for (i = config.page; i < no_of_pages; i++) {
            page_nav += "<li><a data-page=" + i + ">" + i + "</a></li>\n";
        }
        return page_nav;
    }
    function build_results() {
        var i,
            tmp = "",
            start = (config.page !== 1) ? config.page * config.max_per_page : 1,
            end = start + config.max_per_page,
            result;
        if(start > 1) end++;
        for (i = start; i < end; i++) {
            result = results[i];
            if (typeof result !== "undefined") {
                tmp += "<p>" + result + "</p>\n";
                SearchId(config.page);

            }
            else {
                tmp = "";
            }
        }
        return tmp;
    }
    function update_page() {
        document.getElementById("curr_page").innerText = config.page;
        document.getElementById("page_nav").innerHTML = build_nav();
        document.getElementById("results").innerHTML = build_results();
    }
    window.addEventListener("load", function () {
        init();
    });
})();

//Pagination functions ends


//Search author starts...
var authorId = "";
function SearchAuthor() {
    var input, filter;
    input = document.getElementById("search-inputAuthor");
    filter = input.value;//.toLowerCase();

    req = new XMLHttpRequest();
    var link = 'https://jsonplaceholder.typicode.com/users';
    var findByAuthor = '?name=' + filter;

    req.open("GET", link + findByAuthor, true);
    req.send();
    req.onload = function () {
        json = JSON.parse(req.responseText);
        json.forEach(function (val) {
            var keys = Object.keys(val);
            keys.forEach(function (key) {
                if (key === 'id') {
                    authorId = val[key];
                }
            });
        });
    }

    req1 = new XMLHttpRequest();
    var link = 'https://jsonplaceholder.typicode.com/posts';
    var findByAuthor = '?userId=' + authorId;

    req1.open("GET", link + findByAuthor, true);
    req1.send();
    req1.onload = function () {
        json = JSON.parse(req1.responseText);
        var html = "";
        json.forEach(function (val) {
            var keys = Object.keys(val);
            html += "<div class = 'post'>";
            html += "<strong>" + "Author" + "</strong>: " + filter + "<br>";
            keys.forEach(function (key) {
                html += "<strong>" + key + "</strong>: " + val[key] + "<br>";
            });
            html += "</div><br>";
        });

        document.getElementsByClassName('post-list')[0].innerHTML = html;
    }
}
//Search author ends...

//Search title starts
function SearchTitle() {
    var input, filter;
    input = document.getElementById("search-inputTitle");
    filter = input.value.toLowerCase();

    req = new XMLHttpRequest();
    var link = 'https://jsonplaceholder.typicode.com/posts';
    var findByTitle = '?title=' + filter;

    req.open("GET", link + findByTitle, true);
    req.send();
    req.onload = function () {
        json = JSON.parse(req.responseText);
        var html = "";
        json.forEach(function (val) {
            var keys = Object.keys(val);
            html += "<div class = 'post'>";
            keys.forEach(function (key) {
                html += "<strong>" + key + "</strong>: " + val[key] + "<br>";
            });
            html += "</div><br>";
        });
        document.getElementsByClassName('post-list')[0].innerHTML = html;
    }
}
//Search title ends..

///Search body starts...
function SearchBody() {
    var input, filter;
    input = document.getElementById("search-inputBody");
    filter = input.value.toLowerCase();

    req = new XMLHttpRequest();
    var link = 'https://jsonplaceholder.typicode.com/posts';
    var findByBody = '?body=' + filter;

    req.open("GET", link + findByBody, true);
    req.send();
    req.onload = function () {
        json = JSON.parse(req.responseText);
        var html = "";
        json.forEach(function (val) {
            var keys = Object.keys(val);
            html += "<div class = 'post'>";
            keys.forEach(function (key) {
                html += "<strong>" + key + "</strong>: " + val[key] + "<br>";
            });
            html += "</div><br>";
        });
        document.getElementsByClassName('post-list')[0].innerHTML = html;
    }

}
//Search body ends...



//Starts author details function..
function SearchId(filter) {
    var input, authorId;

    req = new XMLHttpRequest();
    var link = 'https://jsonplaceholder.typicode.com/users';
    var findByAuthor = '?id=' + filter;
    req.open("GET", link + findByAuthor, true);
    req.send();
    req.onload = function () {
        json = JSON.parse(req.responseText);
        html = "";
        json.forEach(function (val) {
            var keys = Object.keys(val);
            html += "<div class = 'post'>";
            html += "<strong>Author:</strong>";

            keys.forEach(function (key) {
                //html += "<strong>" + key + "</strong>: " + val[key] + "<br>";
                if (key === 'name') {
                    html += val[key];
                }

            });
            html += "</div><br>";
        });
        document.getElementsByClassName('post-list1')[0].innerHTML = html;

        // return html;
    }

}

//Ends author info function


//Delete() function starts...
function Delete(delete_input) {
    var item = document.getElementById('delete_input');
    var link = "https://jsonplaceholder.typicode.com/posts/" + item;
    fetch(link, {
        method: 'DELETE'
    })
}
//Delete function ends

//Edit() function starts...
function Edit() {
    var userId1 = document.getElementById('userId_input');
    var id1 = document.getElementById('id_input');
    var title1 = document.getElementById('title_input');
    var body1 = document.getElementById('body_input');
    var link = 'https://jsonplaceholder.typicode.com/posts/1' + userId1;
    fetch(link, {
        method: 'PUT',
        body: JSON.stringify({
            id: id1,
            title: title1,
            body: body1,
            userId: userId1
        }),
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        }
    })
        .then(response => response.json())
    //.then(json => console.log(json))

}
//Edit() function ends
